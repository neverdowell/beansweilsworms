﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Team {
    public string name = "New Team";
    public Color decorationColor = Color.white;
    public List<PlayerTeamTest> players = new List<PlayerTeamTest>();
    public int currentPlayer = 0;




    #region helper
    public void NextPlayer() {
        currentPlayer = (currentPlayer + 1) % players.Count;
    }

    public GameObject GetCurrentPlayer() {
        return players[currentPlayer].gameObject;
    }

    public void SetPlayerInputActive(bool activeInput) {
        players[currentPlayer].isActivePlayer = activeInput;
    }
    #endregion

}
