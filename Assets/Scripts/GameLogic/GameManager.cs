﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManager : MonoBehaviour {

    public GameSettings gameSettings;
    public int currentTeam = 0;
    public List<Team> teams = new List<Team>();
    public OrbitCamera orbitCamera; // Player's follow OrbitCamera

    [SerializeField]
    private GameObject planetsParent;
    [SerializeField]
    private GameObject player;




    #region singleton
    private static GameManager instance;
    private GameManager() { }

    public static GameManager GetInstance() {
        return instance;
    }

    #endregion




    #region lifecycle
    private void Awake() {
        if (instance == null) {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
            SpawnPlayers(3, 5);
            teams[currentTeam].SetPlayerInputActive(false); // enable first player's movement
        } else {
            if (instance != this) {
                Destroy(this.gameObject);
            }
        }
    }

    private void Update() {
        // test
        if (Input.GetKeyDown(KeyCode.Tab)) {
            NextTeam();
        } else if (Input.GetKeyDown(KeyCode.Return)) {
            NextPlayer();
        }
        // test end
    }
    #endregion

    #region helper
    public void ExitGame() {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_STANDALONE
        Application.Quit();
#endif
    }

    public void SpawnPlayers(int teamAmount, int playerPerTeam)
    {
        int planChildCount = planetsParent.transform.childCount;

        GameObject teamsObj = new GameObject("Teams");
        

        for (int teamNum = 0; teamNum < teamAmount; teamNum++)
        {
            Team team = new Team();
            if (teamNum == 0)
                team.decorationColor = Color.blue;
            else if (teamNum == 1)
                team.decorationColor = Color.red;
            else if (teamNum == 2)
                team.decorationColor = Color.yellow;

            teams.Add(team);

            GameObject teamObj = new GameObject(team.name + " " + teamNum);
            teamObj.transform.SetParent(teamsObj.transform);

            for (int playerNum = 0; playerNum < playerPerTeam; playerNum++)
            {
                int planetNum = Random.Range(0, planChildCount - 1);
                GameObject planet = planetsParent.transform.GetChild(planetNum).gameObject;

                if (!SpawnPlayer(planet, teamObj, teamNum))
                    playerNum--;

                Debug.Log("Planet: " + planetNum);
            }
        }
    }

    private bool SpawnPlayer(GameObject planet, GameObject parent, int teamNum)
    {
        float planetRadius = planet.GetComponent<GravitySphere>().planetRadius;
        Vector3 spawnLocation = planet.transform.position + Random.onUnitSphere * planetRadius;

        if (Physics.Raycast(spawnLocation, -CustomGravity.GetUpAxis(spawnLocation), out RaycastHit hit))
        {
            if (Physics.OverlapBox(hit.point, player.transform.localScale / 2, hit.transform.rotation,
                1 << LayerMask.NameToLayer("Collider")).Length > 0)
            {
                Debug.Log("Hit");
                return false;
            }
            else
            {
                player.transform.up = hit.normal;

                Vector3 spawn = hit.point + hit.normal * 2;

                GameObject p = Instantiate(player, spawn, player.transform.rotation);

                p.transform.GetChild(0).GetChild(0).GetComponent<Renderer>().material.SetColor("_Color", teams[teamNum].decorationColor);
                p.transform.SetParent(parent.transform);

                teams[teamNum].players.Add(p.GetComponent<PlayerTeamTest>());

                Debug.Log("Player Spawned");
            }

        }

        return true;
    }

    public void NextTeam() {
        teams[currentTeam].SetPlayerInputActive(false); // disable movement of old player
        currentTeam = (currentTeam + 1) % teams.Count;
        SetOrbitCameraTarget();
        teams[currentTeam].SetPlayerInputActive(true); // enabeld movement of new player
    }

    public void NextPlayer() {
        teams[currentTeam].SetPlayerInputActive(false); // disable movement of old player
        teams[currentTeam].NextPlayer();
        SetOrbitCameraTarget();
        teams[currentTeam].SetPlayerInputActive(true); // enabeld movement of new player
    }

    public void SetOrbitCameraTarget() {
        orbitCamera.target = teams[currentTeam].GetCurrentPlayer().transform;
    }

    #endregion
}