﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class TestUIScript : MonoBehaviour
{
    public void logausgabe() {
        Debug.Log("test");
    }

    public void changeScene(string sceneName) {
        SceneManager.LoadSceneAsync(sceneName);
    }

    public void sliderChange(float value) {
        Debug.Log("slider: " + value);
    }
}
