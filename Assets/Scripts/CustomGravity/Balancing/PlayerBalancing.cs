﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BeansWeilWorms/PlayerData")]
public class PlayerBalancing : ScriptableObject
{
	public float maxSpeed = 10f;
	public float maxAcceleration = 10f;
	public float maxAirAcceleration = 1f;
	public float jumpHeight = 2f;
	public int maxAirJumps = 0;
	public float maxGroundAngle = 25f;
	public float maxStairsAngle = 50f;
	public float maxSnapSpeed = 100f;
	public float probeDistance = 1f;
}
