﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugInfo : MonoBehaviour
{
    void Start()
    {
        
    }

    void Update()
    {
        Vector3 gravity = CustomGravity.GetGravity(transform.position);
        float sum = Mathf.Abs(gravity.x) + Mathf.Abs(gravity.y) + Mathf.Abs(gravity.z);
        Debug.Log( "Gravity: "+ gravity + " Sum: " + sum);
    }
}
