﻿using UnityEngine;

public class PlayerSpawn : MonoBehaviour
{
    [SerializeField]
    GameObject player;

    private void Start()
    {
        float planetRadius = GetComponent<GravitySphere>().planetRadius;

        for (int i = 0; i < 20; i++)
        {
            Vector3 spawnLocation = transform.position + Random.onUnitSphere * planetRadius;

            if (Physics.Raycast(spawnLocation, -CustomGravity.GetUpAxis(spawnLocation), out RaycastHit hit))
            {
                
                if (Physics.OverlapBox(hit.point, player.transform.localScale / 2, hit.transform.rotation, 1 << LayerMask.NameToLayer("Collider")).Length > 0)
                    Debug.Log("Hit");
                else
                {
                    player.transform.up = hit.normal;
                    Instantiate(player, hit.point, player.transform.rotation);
                }

            }
        }
    }
}
