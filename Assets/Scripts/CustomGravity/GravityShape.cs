﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityShape : GravitySource
{
    [SerializeField]
    private float gravity = 9.81f;

    [SerializeField, Min(0f)]
    private float outerRadius = 10f, outerFalloffRadius = 15f;

    private float outerFalloffFactor;

    private MeshCollider collider;
    private Vector3[] normals;

    [SerializeField, Min(1)]
    private int reducedDebugLines = 1;

    public override Vector3 GetGravity(Vector3 position)
    {
        Vector3 vector = transform.position - position;
        float distance = vector.magnitude;

        Vector3 pos = transform.position;
        float g = gravity / distance;

        if (Physics.Raycast(position, pos - position, out RaycastHit hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Gravity Shape")))
        {
            // Funtioniert auch ohne, nur dass der radius vom Gravitatons Feld dann ungenauer ist.
            MeshCollider meshCollider = hit.collider as MeshCollider;
            Vector3 vertexPos = Vector3.zero;

            if (!(meshCollider == null || meshCollider.sharedMesh == null))
            {
                vertexPos = transform.localToWorldMatrix.MultiplyPoint3x4(
                    meshCollider.sharedMesh.vertices[meshCollider.sharedMesh.triangles[hit.triangleIndex * 3]]);
            }

            float outerDistance = (vertexPos - transform.position).magnitude;
            // ende 

            if (outerDistance + outerFalloffRadius < distance)
                return Vector3.zero;

            // Debug.DrawRay(position, pos - position, Color.green);

            if (distance > outerRadius + outerDistance)
            {
                g *= -(1f - (distance - outerRadius) * outerFalloffFactor);
            }

            Debug.DrawRay(position, hit.point - position, Color.magenta);

            return (g * (-hit.normal));
        }

        return Vector3.zero;
    }

    void OnValidate()
    {
        collider = GetComponent<MeshCollider>();
        normals = collider.sharedMesh.normals;

        outerFalloffRadius = Mathf.Max(outerFalloffRadius, outerRadius);
        outerFalloffFactor = 1f / (outerFalloffRadius - outerRadius);
    }

    void OnDrawGizmosSelected()
    {
        int i = 0;
        foreach (Vector3 normal in normals)
        {
            if (i % reducedDebugLines != 0)
            {
                i++;
                continue;
            }

            Vector3 world_v = transform.localToWorldMatrix.MultiplyPoint3x4(collider.sharedMesh.vertices[i]);
            Vector3 outerPos = world_v + (normal * outerRadius);
            Vector3 outerFalloffPos = world_v + (normal * outerFalloffRadius);

            Gizmos.color = Color.red;
            Gizmos.DrawLine(world_v, outerPos);

            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(outerPos, outerFalloffPos);
            i++;
        }
    }
}
