﻿using UnityEngine;

public class GravityLine : GravitySource {

    [Header("Configuration")]
    public Transform[] linePoints;
    public Transform point;

    [Header("Gravity")]
    [SerializeField]
    float gravity = 9.81f;
    [SerializeField, Min(0f)]
    float range = 1f;

    [Header("Info/Debug")]
    public Vector3 projectedPoint;


    #region lifecycle
	void OnDrawGizmos () {
        Gizmos.color = Color.yellow;
        if (linePoints.Length > 1) {
            for (int i = 0; i < linePoints.Length - 1; i++) {
                Gizmos.DrawWireSphere(linePoints[i].position, range);
                Gizmos.DrawLine(linePoints[i].position, linePoints[i + 1].position);
                Gizmos.DrawSphere(GetGravity(point.position), 1);
            }
        }
	}
    #endregion




    #region helper
    public override Vector3 GetGravity(Vector3 position)
    {
        //Vector3 up = transform.up;
        //float distance = Vector3.Dot(up, position - transform.position);
        //if (distance > range) {
        //	return Vector3.zero;
        //}
        //float g = -gravity;
        //if (distance > 0f) {
        //	g *= 1f - distance / range;
        //}
        //return g * up;
        projectedPoint =  GetClosestPointToLine(linePoints, position);
        return projectedPoint;
    }

    public Vector3 GetClosestPointToLine(Transform[] linePoints, Vector3 point) {
        float minimumDistance = float.MaxValue;
        float currentDistance = float.MaxValue;
        Vector3 closestPoint = Vector3.zero;
        if (linePoints.Length == 0){
            Debug.LogWarning("Not enough points. At least 1 is required. No force returned (Vector3.zero).");
            return Vector3.zero;
        } else if (linePoints.Length == 1) {
            
            return linePoints[0].position;
        }
        for (int i = 0; i < linePoints.Length - 1; i++) {
            Vector3 projectedPoint = ProjectPointOnLine(linePoints[i].position, linePoints[i + 1].position, point);
            projectedPoint = helper(linePoints[i].position, linePoints[i + 1].position, projectedPoint);
            currentDistance = Vector3.Distance(point, projectedPoint);
            if (currentDistance < minimumDistance) {
                minimumDistance = currentDistance;
                closestPoint = projectedPoint;
            }
        }
        return closestPoint;
    }

    public Vector3 ProjectPointOnLine(Vector3 lineStart, Vector3 lineEnd, Vector3 point) {
        return Vector3.Project((point - lineStart), (lineEnd - lineStart)) + lineStart;
    }


    public Vector3 helper(Vector3 start, Vector3 end, Vector3 projectedPoint) {
        float lineLength = (start - end).magnitude;                 // Distance from start to end (line length)
        float startToPoint = (start - projectedPoint).magnitude;    // Distance from start to projected point
        float endToPoint = (end - projectedPoint).magnitude;        // Distance from projected point to end
        Debug.Log(lineLength + " " + startToPoint + " " + endToPoint);

        if (startToPoint > lineLength) { // if distance greater, projected point is behind end point
            return end;
        }
        else if (endToPoint > lineLength) { // if distance greater, projected point is before start point 
            return start;
        }
        else {
            return projectedPoint;  // projected is between start and end
        }
        
    }

    #endregion
}

/*
using System.Collections.Generic;
using UnityEngine;

public class PointProjection : MonoBehaviour {

    [Header("Info")]
    public Transform[] linePoints;
    //public Transform lineStart;
    //public Transform lineEnd;
    public Transform point;

    [Header("Info")]
    public Vector3 projectedPoint;

    // Start is called before the first frame update
    void Start() {
        //ProjectPoint(lineStart.position, lineEnd.position, point.position);
        Debug.Log(GetClosestPointToLine(linePoints, point));
    }

    // Update is called once per frame
    void Update() {
        projectedPoint = GetClosestPointToLine(linePoints, point);
    }




}

 */