﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Text_Fader : MonoBehaviour {
 
 public float speedFade;
 private float count;


 
 // Use this for initialization
 void Start () {
 
 }
 
 // Update is called once per frame
 void Update () {
  
  Text text = GetComponent<Text>();
  //Fade in-out press start
  count += speedFade * Time.deltaTime;
  
  text.color = new Color(1,1,1,Mathf.Sin(count)*1);
  
 }
}
