using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Wait_Intro : MonoBehaviour
{
    public float wait_Time = 5;
    void Start()
    {
        StartCoroutine(Wait_for_Intro());
    }

    IEnumerator Wait_for_Intro()
    {
        yield return new WaitForSeconds(wait_Time);

        SceneManager.LoadScene(1);
    }
}
