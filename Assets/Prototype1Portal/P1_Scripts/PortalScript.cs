﻿using UnityEngine;

public class PortalScript : MonoBehaviour{
    public GameObject PortalTexturePlane;
    public GameObject PortalCollider;
    public GameObject PortalSpawnPoint;
}
