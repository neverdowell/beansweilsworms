﻿using UnityEngine;

public class P1_PortalScript : MonoBehaviour{
    [Header("Settings")]
    public GameObject playerCamera;
    public GameObject portalCamera;
    public GameObject senderPortal;
    public GameObject receiverPortal;

    //public Vector3 portalCameraOffset;




    #region lifecycle
    void Update() {
        P1_PortalSystem.teleportGameObjectByPortals(playerCamera, portalCamera, senderPortal, receiverPortal);
    }
    #endregion
}
