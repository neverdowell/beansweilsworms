﻿using UnityEngine;

public class P1_PlayerScript : MonoBehaviour{
    [Header("Balancing Data")]
    public float movementSpeed;
    public float rotationSpeed;

    [Header("Player Input")]
    public KeyCode forwardKeyCode = KeyCode.W;
    public KeyCode strafeLeftKeyCode = KeyCode.A;
    public KeyCode strafeRightKeyCode = KeyCode.D;
    public KeyCode backKeyCode = KeyCode.S;
    public KeyCode rotateLeftKeyCode = KeyCode.Q;
    public KeyCode rotateRightKeyCode = KeyCode.E;



    void Start(){
        
    }

    void Update(){
        float moveModifier = 0;
        float rotationModifer = 0;
        float strafeModifier = 0;

        // rotation input
        if (Input.GetKey(rotateLeftKeyCode)) {
            rotationModifer = -1;
        }
        if (Input.GetKey(rotateRightKeyCode)) {
            rotationModifer = 1;
        }
        // actually rotate (before move)
        transform.Rotate(Vector3.up * rotationModifer * rotationSpeed * Time.deltaTime);


        // move input
        if (Input.GetKey(forwardKeyCode)) {
            moveModifier = 1;
        }
        if (Input.GetKey(backKeyCode)) {
            moveModifier = -1;
        }
        if (Input.GetKey(strafeLeftKeyCode)) {
            strafeModifier = -1;
        }
        if (Input.GetKey(strafeRightKeyCode)) {
            strafeModifier = 1;
        }
        // actually move and strafe (after rotation)
        transform.Translate((Vector3.forward * moveModifier  + Vector3.right * strafeModifier).normalized * movementSpeed * Time.deltaTime);
    }
}
