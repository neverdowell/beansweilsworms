﻿// Inpired by https://www.youtube.com/watch?v=cuQao3hEKfs (https://github.com/Brackeys/Portal-In-Unity)

using UnityEngine;

public enum Portal {
    SENDER,
    RECEIVER
}

public class P1_PortalSystem : MonoBehaviour{
    [Header("Settings")]
    public GameObject playerCamera;// portal script
    //public GameObject portalCamera; // portal script
    //public GameObject senderPortal; // portal script
    //public GameObject receiverPortal; // portal script

    public GameObject portalPrefab;
    public GameObject portalSpawnpoint;

    [Header("Config")]
    public int renderTextureBitDepth = 24;
    public Shader unlitCutOutShader;

    public GameObject[] portals = new GameObject[2];
    public Camera[] portalCameras;
    public RenderTexture[] portalRenderTextures;
    public Material[] portalRenderTextureMaterials;


    // private
    private int numberOfPortalsSpawned = 0;
    private int lastScreenWidth = 0;
    private int lastScreenHeight = 0;




    #region lifecycle
    private void Awake() {
        InitPortalSystem();
    }

    private void Start() {
        // test
        SpawnPortal(0, new Vector3(0, 0, 0), Quaternion.identity);
        SpawnPortal(1, new Vector3(-10, 0, 0), Quaternion.identity);
    }

    private void Update() {
        // check if screen resolution has changed
        if (lastScreenWidth != Screen.width || lastScreenHeight != Screen.height) {
            // store screensize
            lastScreenWidth = Screen.width;
            lastScreenHeight = Screen.height;
            // resize render textures of all portal cameras
            resizeAllPortalCameraTargetTextures(); 
        }
        // Move PortalCameras with PlayerCamera
        P1_PortalSystem.teleportGameObjectByPortals(playerCamera, portalCameras[0].gameObject, portals[0], portals[1]);
        
    }

    private void OnGUI() {
        GUI.contentColor = Color.black;
        GUI.Label(new Rect(new Vector2(10, 10), new Vector2(200, 20)), "Move with WASD");
        GUI.Label(new Rect(new Vector2(10, 30), new Vector2(200, 20)), "Rotate with Q and E");
    }
    #endregion



    #region spawn portal
    public void InitPortalSystem() {
        portals = new GameObject[2];
        portalCameras = new Camera[2];
        portalRenderTextures = new RenderTexture[2];
        portalRenderTextureMaterials = new Material[2];
        // create portals
        CreatePortal(0);
        CreatePortal(1);
        // connect portals
        ConnectPortals();
        //HidPortals(); // hide initially
    }

    /// <summary>
    /// Creates a portal's GameObject:
    /// * Instantiates Portals Mesh
    /// * Adds a Camera for each Portal
    /// * Creates a new Material with Unlit/Cutout Shader
    /// </summary>
    /// <param name="portalIndex"></param>
    public void CreatePortal(int portalIndex) {
        if (portalPrefab != null) {
            // Create portal from prefab
            portals[portalIndex] = Instantiate(portalPrefab, Vector3.zero, Quaternion.identity);
            portals[portalIndex].name = "Portal" + portalIndex;
            // Create camera GameObject with Component
            GameObject cameraGameObject = new GameObject();
            portalCameras[portalIndex] = cameraGameObject.AddComponent<Camera>();
            portalCameras[portalIndex].name = "PortalCamera" + portalIndex;
            // Create portal materials
            portalRenderTextureMaterials[portalIndex] = new Material(unlitCutOutShader);
            portalRenderTextureMaterials[portalIndex].name = "PortalRenderTextureMaterial" + portalIndex;
            
            // HINT: render texture is created by "ResizePortalCameraTargetTexture"
        } else {
            Debug.LogWarning("PortalSystem.SpawnPortal: Cannot instantiate portal prefab. Protal Prefab is null.");
        }
    }

    /// <summary>
    /// Connects all components for Portal's Plane Materiale
    /// * Sets PortalCamera's render target to a RenderTexture,
    /// * RenderTexture with
    /// </summary>
    public void ConnectPortals() {
        //if (numberOfPortalsSpawned == 2) { // make better check
            portalCameras[0].targetTexture = portalRenderTextures[0]; // camera render to texture
            portalRenderTextureMaterials[0].mainTexture = portalRenderTextures[0];
            // add Material to Plane
            GameObject portalTexturePlane0 = portals[0].transform.GetChild(0).gameObject;
            portalTexturePlane0.GetComponent<MeshRenderer>().material = portalRenderTextureMaterials[0];

            portalCameras[1].targetTexture = portalRenderTextures[1]; // camera render to texture
            portalRenderTextureMaterials[1].mainTexture = portalRenderTextures[1];
            // add Material to plane
            GameObject portalTexturePlane1 = portals[1].transform.GetChild(0).gameObject;
            portalTexturePlane1.GetComponent<MeshRenderer>().material = portalRenderTextureMaterials[1];
        //}
    }

    public void SpawnPortal(int portalIndex, Vector3 position, Quaternion rotation) { // maybe add scale later on
        portals[portalIndex].transform.position = position;
        portals[portalIndex].transform.rotation = rotation;
        portals[portalIndex].SetActive(true);
    }

    public void DespawnPortal(int portalIndex) {
        portals[portalIndex].SetActive(false);
        // set Cameras inactive too
    }
    #endregion




    #region resize render texture sizes
    public void resizeAllPortalCameraTargetTextures() {
        for (int i = 0; i < portalCameras.Length; i++) {
            ResizePortalCameraTargetTexture(i);
        }
    }

    public void ResizePortalCameraTargetTexture(int portalCameraIndex) {
        Camera portalCamera = portalCameras[portalCameraIndex];
        Material portalCameraMaterial = portalRenderTextureMaterials[portalCameraIndex];
        portalRenderTextures[portalCameraIndex] = new RenderTexture(lastScreenWidth, lastScreenHeight, renderTextureBitDepth);
        if (portalCamera.targetTexture != null) {
            portalCamera.targetTexture.Release();
        }
        portalCamera.targetTexture = portalRenderTextures[portalCameraIndex];
        portalCameraMaterial.mainTexture = portalRenderTextures[portalCameraIndex];
    }
    #endregion




    #region static teleportation
    // Original post: https://answers.unity.com/questions/1118179/portals-transform-rotation-of-ported-objects.html
    /// <summary>
    /// Set position and rotation relatively equal as if portals were a mirror.
    /// Can be used to set a portal's camera position/rotation or teleport objects through a portal
    /// </summary>
    /// <param name="originGameObject">GameObject to be teleported</param>
    /// <param name="targetGameObject">GameObject to teleported position and rotation</param>
    /// <param name="originPortal">Sender portal, which you'd walk in</param>
    /// <param name="targetPortal">Receiver portal, where you come out</param>
    public static void teleportGameObjectByPortals (GameObject originGameObject, GameObject targetGameObject, GameObject originPortal, GameObject targetPortal) {
        // porting position
        targetGameObject.transform.position = teleportPositionByPortals(originGameObject, originPortal, targetPortal);

        // Porting the rotation(you maybe have to rotate the object additionally 180° around portalB.transform.up):
        targetGameObject.transform.rotation = teleportQuaternionByPortals(originGameObject, originPortal, targetPortal);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="originGameObject"></param>
    /// <param name="targetGameObject"></param>
    /// <param name="originPortal"></param>
    /// <param name="targetPortal"></param>
    public static void teleportRigidbodyVelocityByPortals(GameObject originGameObject, GameObject targetGameObject, GameObject originPortal, GameObject targetPortal ) {
        Rigidbody originRigidbody = originGameObject.GetComponent<Rigidbody>();
        Rigidbody targetRigidbody = targetGameObject.GetComponent<Rigidbody>();
        targetRigidbody.velocity = teleportDirectionByPortals(originRigidbody.velocity, originPortal, targetPortal);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="originDirection"></param>
    /// <param name="originPortal"></param>
    /// <param name="targetPortal"></param>
    /// <returns></returns>
    public static Vector3 teleportDirectionByPortals(Vector3 originDirection, GameObject originPortal, GameObject targetPortal ) {
        return targetPortal.transform.TransformDirection(originPortal.transform.InverseTransformDirection(originDirection));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="originGameObject"></param>
    /// <param name="originPortal"></param>
    /// <param name="targetPortal"></param>
    /// <returns></returns>
    public static Vector3 teleportPositionByPortals( GameObject originGameObject, GameObject originPortal, GameObject targetPortal ) {
        return targetPortal.transform.TransformPoint(originPortal.transform.InverseTransformPoint(originGameObject.transform.position));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="originGameObject"></param>
    /// <param name="originPortal"></param>
    /// <param name="targetPortal"></param>
    /// <returns></returns>
    public static Quaternion teleportQuaternionByPortals( GameObject originGameObject, GameObject originPortal, GameObject targetPortal ) {
        return targetPortal.transform.rotation * Quaternion.Inverse(originPortal.transform.rotation) * originGameObject.transform.rotation;
    }
    #endregion
}
