﻿// Inpired by https://www.youtube.com/watch?v=cuQao3hEKfs and https://github.com/Brackeys/Portal-In-Unity

using UnityEngine;

public class P1_PortalTeleporter : MonoBehaviour{

	public GameObject player;
	public GameObject receiverPortalSpawnpoint;

	private bool playerIsOverlapping = false;

	// Update is called once per frame
	void Update() {
		if (playerIsOverlapping) {
			Vector3 portalToPlayer = player.transform.position - transform.position;
			float dotProduct = Vector3.Dot(transform.up, portalToPlayer);
			if (dotProduct < 0f) {
				player.transform.position = P1_PortalSystem.teleportPositionByPortals(player, gameObject, receiverPortalSpawnpoint);
				player.transform.rotation = P1_PortalSystem.teleportQuaternionByPortals(player, gameObject, receiverPortalSpawnpoint);
				playerIsOverlapping = false;
				// destroy portals
			}
		}
	}

	void OnTriggerEnter( Collider other ) {
		if (other.tag == "Player") {
			playerIsOverlapping = true;
		}
	}

	void OnTriggerExit( Collider other ) {
		if (other.tag == "Player") {
			playerIsOverlapping = false;
		}
	}
}