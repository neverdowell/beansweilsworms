# Abstract
BeansWeilWorms ist ein Worms3D-Klon mit einer MarioGalaxy-Physik und unterschiedlichen Planeten. Mehr Infos siehe [Wiki](https://gitlab.com/neverdowell/beansweilsworms/-/wikis/home).

# git-Manual

## Installation
* Kommandozeile/Bash öffnen
* `git version` eintippen. Gibt es eine Fehlermeldung, muss git noch installiert werden
* Download auf https://git-scm.com
* Installation (dabei alle Abfragen mit OK bestätigen)
* Erneuter Test mit `git version`

## Aktuellen Stand des Projekts prüfen
* `git status`

## Projekt erstmalig herunterladen
* `git clone https://gitlab.com/neverdowell/beansweilsworms`

## Neuesten Stand des Projekts herunterladen
* `git pull`

## Eigene Änderungen hochladen
NIEMALS wird ungetesteter Code hochgeladen! Immer wird zuerst getestet.
* `git add .`
* `git commit -m "KOMMENTAR"`
* `git push`

## Änderungen zum letzten commit rückgängig machen
* `git reset --hard`
* `git clean --force`

